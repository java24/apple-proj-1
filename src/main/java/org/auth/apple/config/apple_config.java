package org.auth.apple.config;

import com.github.mikereem.apple.signin.util.AppleSigninUtil;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Objects;

@Configuration
public class apple_config

{
    @Value("${apple.auth.key.id}")
    private String Apple_Key_Id;

    @Value("${apple.auth.Team.id}")
    private String Apple_Team_Id;

    public void init() {

        AppleSigninUtil appleSignin = new AppleSigninUtil();
        try {
            AppleSigninUtil.init(Apple_Key_Id, Apple_Team_Id, new InputStreamReader(
                    Objects.requireNonNull(getClass().getClassLoader().getResourceAsStream("META-INF/AuthKey_LAXP3KSM5L.p8"))));
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }
    }

}
