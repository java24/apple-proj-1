package org.auth.apple;

import org.apache.log4j.BasicConfigurator;
import org.auth.apple.services.Auth_login;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.logging.Logger;
@EnableAutoConfiguration
@SpringBootApplication
public class AppleAuthApplication {
	private Logger logger = Logger.getLogger(String.valueOf(Auth_login.class));
	public static void main(String[] args) {
		BasicConfigurator.configure();
		SpringApplication.run(AppleAuthApplication.class, args);
	}

}
